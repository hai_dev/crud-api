import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { UserDeLeteComponent } from './user-delete/user-delete.component';
import { UserAddComponent } from './user-add/user-add.component';
import { UserEditComponent } from './user-edit/user-edit.component';

const routes: Routes = [
  {
    path: 'users',
    component: UsersComponent,
    data: {title: 'List of Users'},
  },
  {
    path: 'user-delete/:id',
    component: UserDeLeteComponent,
    data: {title: 'List of Users'},
  },
  {
    path: 'user-add',
    component: UserAddComponent,
    data: {title: 'List of Users'},
  },
  {
    path: 'user-edit/:id',
    component: UserEditComponent,
    data: {title: 'List of Users'},
  },
  {
    path: '',
    redirectTo: '/users',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
