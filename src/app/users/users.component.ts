import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { User } from '../user'
import { Router, ActivatedRoute } from '@angular/router';
import {DataSource} from '@angular/cdk/table';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'age'];
  userName: string;
  data: User[] = [];
  user: User = new User();
  apiUrl = "http://localhost:3000/users"
  isLoadingResults = true;
  

  constructor(private http:HttpClient, private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.getUsers();
  };
  
  getUsers (){
    return this.http.get(this.apiUrl).subscribe((data: any[]) => {this.data = data; console.log(data)});
  };
}