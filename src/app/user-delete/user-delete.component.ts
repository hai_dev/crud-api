import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Router, ActivatedRoute} from '@angular/router';
import {User} from '../user';
@Component({
  selector: 'app-user-delete',
  templateUrl: './user-delete.component.html',
  styleUrls: ['./user-delete.component.css']
})
export class UserDeLeteComponent implements OnInit {
  apiUrl: string = "http://localhost:3000/users";
  user: User = new User();
  id: string;
  constructor(
    private http: HttpClient, 
    private router: Router, 
    private route: ActivatedRoute
    ){};

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get("id");
    console.log(id);
    this.id = id;
    this.getUser(id);
  }
  getUser(id: string){
    this.http.get(`${this.apiUrl}/${id}`).subscribe((user: User) =>{
      this.user = user;
      console.log(user);
    });
  }
  deleteUser(){
    this.http.delete(`${this.apiUrl}/${this.id}`).subscribe(() => {
      this.router.navigate(['/users']);
    })
  }
}
