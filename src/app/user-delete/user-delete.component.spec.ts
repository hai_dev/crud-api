import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDeLeteComponent } from './user-delete.component';
import { User } from '../user';

describe('UserDeleteComponent', () => {
  let component: UserDeLeteComponent;
  let fixture: ComponentFixture<UserDeLeteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDeLeteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDeLeteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
