import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {User} from '../user';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  user: User = new User();
  apiUrl: string = "http://localhost:3000/users";
  constructor(private router: Router, private route:ActivatedRoute, private http:HttpClient) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get("id");
    console.log(id);
    this.editUser(id);
    // this.updateUser();
  }

  editUser(id: string){
    this.http.get(`${this.apiUrl}/${id}`).subscribe((user: User) =>{
      this.user = user;
      console.log(user);
    });
  }
  
  updateUser(){
    this.http.put(this.apiUrl, this.user).subscribe(() =>{
      this.router.navigate(['/users']);
    });
  };
}
