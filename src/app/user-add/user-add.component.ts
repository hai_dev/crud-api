import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { User } from '../user'
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent implements OnInit {
  apiUrl: string = "http://localhost:3000/users"
  data: User[] = [];

  user: User = new User(); 
  
  constructor(private http:HttpClient, private router: Router) { }

  ngOnInit() {
  }
  addUser(){
    this.http.post(this.apiUrl, this.user).subscribe(() =>{
      this.router.navigate(['/users'])
    })
  }
}
